import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase';

Vue.config.productionTip = false
let app = '';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBnCPWEyoFYI2gFS-yxOD5OCXQ8HpdOsC4",
    authDomain: "vimixagram-firebase.firebaseapp.com",
    databaseURL: "https://vimixagram-firebase.firebaseio.com",
    projectId: "vimixagram-firebase",
    storageBucket: "gs://vimixagram-firebase.appspot.com",
    messagingSenderId: "407358209270"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(() => {
    if (!app) {
        app = new Vue({
            router,
            render: h => h(App)
        }).$mount('#app');
    }
})